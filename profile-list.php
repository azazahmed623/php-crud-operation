<?php include('header.php'); ?>

<!-- Table Start  -->
<div class="table-area ">
  <div class="container-fluid pt-5">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="category-list.php">Profile</a></li>
        <li class="breadcrumb-item active" aria-current="page">Profile-list</li>
      </ol>
    </nav>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-7 d-flex">
        <div class="cat-list-left">
          <h2>Profile List</h2>
        </div>
        <div class="search-form px-5">
          <form action="" method="POST">
            <tr>
              <input type="text" name="search_name" placeholder="Search by username">
              <input type="submit" name="search" value="Search">
            </tr>
          </form>
        </div>
      </div>
      <div class="col-md-5">
        <div class="cat-list-right">

          <?php

          //session_start();

          if (isset($_SESSION['addd'])) {
            session_start();
            echo $_SESSION['addd']; //Displaying Session Message
            unset($_SESSION['addd']); //Removing Session Message
          }

          if (isset($_SESSION['delete'])) {
            session_start();
            echo $_SESSION['delete']; //Displaying Session Message
            unset($_SESSION['delete']); //Removing Session Message
          }


          if (isset($_SESSION['update'])) {
            session_start();
            echo $_SESSION['update']; //Displaying Session Message
            unset($_SESSION['update']); //Removing Session Message
          }



          ?>

          <a href="add-profile.php">Add Profile</a>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid pb-3 px-3">

    <!-- <form action="" method="POST"> -->
      <table class="table cat-list-img">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Profile</th>
            <th scope="col">Username</th>
            <th scope="col">Email</th>
            <th scope="col">Password</th>
            <th class="cat-action" scope="col">Action</th>
            <th>
              <!-- <center><input type="submit" name="delete_a" value="D-All"></center> -->
            </th>
          </tr>
        </thead>
        <tbody>
          <?php
 
          $conn = mysqli_connect('localhost', 'root', '', 'rawphp');

          if (!$conn) {
            die("not connection" . mysqli_error($conn));
          }

          if (isset($_REQUEST['search'])) {
            $recv_name = $_REQUEST['search_name'];


            //Query to Get all admin
            $sql = "SELECT * FROM user_list WHERE username LIKE '%$recv_name%'";
            //Execute the Query
            $res = mysqli_query($conn, $sql);
            //Check whether the Query is Executed of not
            if ($res == true) {
              //Count rows to check whether we have data in database or not
              $count = mysqli_num_rows($res); //Function to get all the rows in databse

              $sn = 1; //create a variable and assig

              //check the num of rows
              if ($count > 0) {
                //we have data in database
                while ($rows = mysqli_fetch_assoc($res)) {
                  //using while loop to get all the data form database.
                  //and while loop will run as long as we have data in databse

                  //Get individual Data
                  $id = $rows['id'];
                  $profile_pic = $rows['profile_pic'];
                  $username = $rows['username'];
                  $email = $rows["email"];
                  $password = $rows["password"];

                  //display the values in our table
          ?>

                  <tr>
                    <td><?php echo $sn++; ?></td>
                    <td><img src="images/<?php echo $profile_pic; ?>" style="width: 40px !important;
                    height: 40px !important;
                    border-radius: 50%;" alt=""></td>
                    <td><?php echo $username; ?></td>
                    <td><?php echo $email; ?></td>
                    <td><?php echo $password; ?></td>
                    <td>
                      <a onclick="return confirm('Ary you sure')" class="bt-1" href="delete.php?id=<?php echo $id; ?>&profile_pic=<?php echo $profile_pic; ?>">Delete</a>
                      <a class="bt-2" href="edit-profile.php?edit_id=<?php echo $id; ?>">Edit</a>
                      <a class="bt-3" href="view-category.php">View</a>
                    </td>
                    <!-- <td><center><input type="checkbox" name="check_d" id="chack_d" value="<?php echo $id; ?>"></center></td> -->
                  </tr>

          <?php

                }
              } else {
                //we do not have data in databse
                echo "You don'thave any data in your database";
              }
            }
          }
          ?>
        </tbody>
      </table>
    <!-- </form> -->
  </div>
  </div-table-area>
  <!-- Table End  -->
  <?php include('footer.php'); ?>

  <?php

 

  // if (isset($_POST['delete_a'])) {
  //   $chk_delete = $_POST['check_d'];
  //   $all_marked = implode(",",$chk_delete);

  //   $sql = "DELETE FROM user_list WHERE id in ($all_marked)";

  //   $run_delete_query = mysqli_query($conn, $sql);
  // }

  ?>