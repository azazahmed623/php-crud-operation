<?php include('header.php'); ?>

<!-- View Product Area Start  -->
<div class="product-section section py-4">
  <div class="container-fluid pt-5">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="profile.php">Profile</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit Profile</li>
      </ol>
    </nav>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-12">
        <div class="profile-right">
          <h3>Edit Profile</h3>
          <?php



          $conn = mysqli_connect('localhost', 'root', '', 'rawphp');

          if (!$conn) {
            die("not connection" . mysqli_error($conn));
          }

          if (isset($_REQUEST['edit_id'])) {
            $rec_id = $_REQUEST['edit_id'];

            $get_info = "SELECT * FROM user_list WHERE id = $rec_id";

            $select_info = mysqli_query($conn, $get_info);

            while ($row = mysqli_fetch_assoc($select_info)) {

          ?>

              <form action="update-profile.php" method="post">
                <table>
                  <thead>
                    <tr>
                      <th><label for="username">User Name</label></th>
                      <th><label for="email">Email</label></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><input type="text" name="username" id="username" value="<?php echo $row['username']; ?>"></td>
                      <td><input type="email" name="email" id="email" value="<?php echo $row['email']; ?>"></td>
                    </tr>
                    <tr>
                      <th><label for="password">Password</label></th>
                      <th><label for="newpass">Profile Images</label></th>
                    </tr>
                    <tr>
                      <td><input type="password" name="password" id="password" value="<?php echo $row['password']; ?>"></td>
                      <td><input type="file" name="profileimages" id="profileimages"></td>
                    </tr>
                    <tr>
                      <td><input class="submit" type="submit" name="submit" id="submit" value="Update"></td>
                      <td><input type="hidden" name="updating_hidder_id" value="<?php echo $rec_id; ?>"></td>
                    </tr>
                  </tbody>
                </table>
              </form>

          <?php

            }
          }

          ?>
        </div>
      </div>
    </div>
  </div>
  <!-- View Product Area End  -->

  <?php include('footer.php'); ?>