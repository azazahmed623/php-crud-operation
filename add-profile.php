<?php include('header.php'); ?>

<!-- View Product Area Start  -->
<div class="product-section section py-4">
  <div class="container-fluid pt-5">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="profile.php">Profile</a></li>
        <li class="breadcrumb-item active" aria-current="page">Add Profile</li>
      </ol>
    </nav>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-12">
        <div class="profile-right">
          <h3>Add Profile</h3>
          <form action="" method="post" enctype="multipart/form-data">
            <table>
              <thead>
                <tr>
                  <th><label for="username">User Name</label></th>
                  <th><label for="email">Email</label></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><input type="text" name="username" id="username"></td>
                  <td><input type="email" name="email" id="email"></td>
                </tr>
                <tr>
                  <th><label for="password">Password</label></th>
                  <th><label for="newpass">Profile Images</label></th>
                </tr>
                <tr>
                  <td><input type="password" name="password" id="password"></td>
                  <td><input type="file" name="profileimages" id="profileimages"></td>
                </tr>
                <tr>
                  <td><input class="submit" type="submit" name="submit" id="submit" value="Add Profile"></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- View Product Area End  -->

  <?php

if (isset($_POST['submit'])) {

  //1. Get the Data form form
  $username = $_POST['username'];
  $email = $_POST['email'];
  $password = md5($_POST['password']); //Password Encryption with MD5
  $rec_file = $_FILES['profileimages'];

  $images_name = $rec_file['name'];
  $images_tmp_name = $rec_file['tmp_name'];

  if(!empty($images_name)){
    $loc = "images/";
    move_uploaded_file($images_tmp_name, $loc.$images_name);
  }else{
    echo "Your file is empty.";
  }

  $conn = mysqli_connect('localhost', 'root', '', 'rawphp');

  if (!$conn) {
    die("not connection" . mysqli_error($conn));
  }

  //2. SQL Query to Save the data into database
  $sql = "INSERT INTO user_list SET
  profile_pic='$images_name',
  username='$username',
  email='$email',
  password='$password' 
  ";

  //3. Executing Query and Saving Data into Database
  $res = mysqli_query($conn, $sql) or die(mysqli_error());

  //4. Check whether the (Query is Executed) data is inserted or not and display appropriate message
  if ($res) {
    $_SESSION['addd'] = "Successfull";
    //header("Location: profile-list.php?addd");
  } 
}

?>

  <?php include('footer.php'); ?>

